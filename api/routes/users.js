const express = require('express');
const User = require('./../models/user');
const userController = require('./../controller/users');

const router = express.Router();

router.post('/signup', userController.user_signup);

router.post('/login', userController.login);

module.exports = router;