const express = require('express');
const authCheck = require('./../middelware/checkAuth');
const orderController = require('./../controller/orders');
const router = express.Router();

router.get('/', orderController.get_all_orders);

router.get('/:orderId', orderController.get_order);

router.post('/', authCheck, orderController.create_order);

router.delete('/:orderId', authCheck, orderController.delete_order);

module.exports = router;