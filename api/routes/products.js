const express = require('express');
const multer = require('multer');

const authCheck = require('./../middelware/checkAuth');
const productController = require('./../controller/products');

const router = express.Router();
const storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './images/');
    },
    filename: function(req, file, callback) {
        callback(null, Date.now() + file.originalname);
    }
});

const fileFilter = (req, file, callback) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true);
    } else {
        callback(new Error('only JPEG/PNG images supported'), false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.get('/', productController.get_all_products);

router.get('/:productId', productController.get_product);

router.post('/', authCheck, upload.single('productImage') , productController.post_product);

router.delete('/:productId', authCheck, productController.delete_Product);

router.put('/:productId', authCheck, productController.update_product);

module.exports = router;