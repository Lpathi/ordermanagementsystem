const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.user_signup = (req, res, next) => {
    findUser(req.body.email, req.body.password, res, true);
}

exports.login = (req, res, next) => {
    findUser(req.body.email, req.body.password, res, false);
}

async function findUser(email, password, res, isSignUp) {
    try {
        let user = await User.find({email: email});
        if (user.length >= 1) {
            if (!isSignUp) {
                hashUserData(email, password, res, isSignUp, user[0]);
            } else {
                res.status(409).json({
                    message: 'EmailID alerady exists in system!!'
                });
            }
        } else {
            if (!isSignUp) {
                res.status(401).json({
                    message: 'You are not Authorized!!'
                });
            } else {
                hashUserData(email, password, res, isSignUp, user[0]);
            }
        }
    } catch (error) {
        if (isSignUp) {
            res.status(500).json({
                message: 'Error while fethcing user details!!',
                Error: error.message
            });
        } else {
            res.status(401).json({
                message: 'You are not Authorized!!',
                Error: ''
            });
        }
    }
}

async function hashUserData(email, password, res, isSignUp, user) {
    try {
        if (!isSignUp) {
            let hash = await bcrypt.compare(password, user.password);
            validateUser(hash,user,res);
        } else {
            let hash = await bcrypt.hash(password, parseInt(process.env.hash_satlt_content));
            saveUser(email, hash, res);
        }
    } catch (error) {
        if (isSignUp) {
            res.status(500).json({
                message: 'Error while hashing user details!!',
                Error: error.message
            });
        } else {
            res.status(401).json({
                message: 'You are not Authorized!!',
                Error: ''
            });
        }
    }
}

async function saveUser(email, hash, res) {
    try {
        const user = new User ({
            _id: new mongoose.Types.ObjectId(),
            email: email,
            password: hash
        });
       let userObj = await user.save();
       res.status(201).json({
            message: 'User has been created successfully!!',
            user: user
        });
    } catch (error) {
        res.status(500).json({
            message: 'Error while saving user details!!',
            error: error.message
        });
    }
}

function validateUser(hash, user, res) {
    if (hash) {
        const token = generateToken(user);
        res.status(200).json({
            message: 'Authentication sucessfull!!',
            user: {
                userid: user._id,
                email: user.email
            },
            token: token
        });
    } else {
        res.status(401).json({
            message: 'You are not Authorized!!',
            Error: ''
        });
    }
}

function generateToken(user) {
    return jwt.sign({
        userId: user._id,
        email: user.email
    }, 
    process.env.server_secret_key,
    {
        expiresIn: "1h"
    });
}