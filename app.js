const express = require('express');
const morgan = require('morgan'); //npm package to log all incomming requests
const mongoose = require('mongoose');
const app = express();

app.use(morgan('dev'));// tunnel all the requests to express and morgan will log the data in console
app.use('/images',express.static('images'));
//establish mongoose connection

mongoose.connect("mongodb+srv://" + process.env.mongoDB_UserName + ":" + process.env.mongoDB_pwd + "@samplecluster-a1mnh.mongodb.net/test?retryWrites=true&w=majority", {
    useNewUrlParser :true,
    useUnifiedTopology: true
})
.then(
    () => { console.log('MongoDB Connection established!!') },
    err => { console.log('Error while connecting to mongoDB : ' + error) }
  );

const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/users');

//Handle Cors requests
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    if (req.path === 'OPTIONS') {
        res.header("Access-Control-Allow-Methods","GET, PUT, POST, DELETE, PATCH")
        return res.status(200).json({});
    }
    next();
});

app.use(express.json());
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/users', userRoutes);

//catch any unhandled routes and return 404
app.use((req, res, next) => {
    const error = new Error('Route not found!!')
    error.status = 404;
    next(error);
});

//catch any unhandled error across application
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;